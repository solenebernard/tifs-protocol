# -*- coding: utf-8 -*-

import numpy as np
from multiprocessing import Pool, cpu_count
import os, sys
import argparse
from functools import partial
import shutil
from scipy.signal import correlate
from scipy.fftpack import dct, idct
import time

import tensorflow as tf
from time import time
sys.path.append('./models/')
from SRNet import cnn_model_fn as cnn_model_fn_sr
from XUNet import cnn_model_fn as cnn_model_fn_xu
from statsmodels.distributions.empirical_distribution import ECDF


"""
Tools for steganographic algorithms
"""

def ternary_entropy(p):
    p[p==0]=1
    H = -((p) * np.log2(p))
    Ht = np.sum(H)
    return Ht


def calc_lambda(rho, message_length, n):
    l3 = 1000 # just an initial value
    m3 = message_length + 1 # to enter at least one time in the loop, just an initial value
                            # m3 is the total entropy
    iterations = 0 # iterations counter
    while m3 > message_length:
        """
        This loop returns the biggest l3 such that total entropy (m3) <= message_length
        Stop when total entropy < message_length
        """
        l3 *= 2
        a = np.exp(-l3*(rho-np.min(rho,axis=0)))
        p = a/(np.sum(a,axis=0))
        
        # Total entropy
        m3 = ternary_entropy(p)
        iterations += 1
        if iterations > 10:
            """
            Probably unbounded => it seems that we can't find beta such that
            message_length will be smaller than requested. Ternary search 
            doesn't work here
            """
            lbd = l3
            return lbd


    l1 = 0.0 # just an initial value
    m1 = n # just an initial value
    lbd = 0.0

    alpha = message_length / n # embedding rate
    # Limit search to 30 iterations
    # Require that relative payload embedded is roughly within
    # 1/1000 of the required relative payload
    while (m1 - m3) / n > alpha / 1000 and iterations < 30:
        lbd = l1 + (l3 - l1) / 2 # dichotomy
        a = np.exp(-lbd*(rho-np.min(rho,axis=0)))
        p = a/(np.sum(a,axis=0))
        m2 = ternary_entropy(p) # total entropy new calculation

        if m2 < message_length: # classical ternary search
            l3 = lbd
            m3 = m2
        else:
            l1 = lbd
            m1 = m2
        iterations += 1 # for monitoring the number of iterations
    return lbd

def compute_proba(rho, message_length, n):
    """
    Embedding simulator simulates the embedding made by the best possible 
    ternary coding method (it embeds on the entropy bound). This can be 
    achieved in practice using Multi-layered syndrome-trellis codes (ML STC) 
    that are asymptotically approaching the bound
    """
    lbd = calc_lambda(rho, message_length, n)
    a = np.exp(-lbd*(rho-np.min(rho,axis=0)))
    p = a/(np.sum(a,axis=0))
    return(p)

def embedding_simulator(cover, rho, message_length, in_line=False):
    """
    Embedding simulator simulates the embedding made by the best possible 
    ternary coding method (it embeds on the entropy bound). This can be 
    achieved in practice using Multi-layered syndrome-trellis codes (ML STC) 
    that are asymptotically approaching the bound
    """

    n = cover.size
    p = compute_proba(rho, message_length, n)

    # XXX: to change
    if not in_line:
        randChange = np.random.random_sample((cover.shape[0], cover.shape[1]))
    else:
        randChange = np.random.random_sample(cover.shape[0])
    y = np.copy(cover)
    y[randChange<p[0]] += + 1
    
    y[np.logical_and(randChange >= p[0], randChange < (np.sum(p[:2],axis=0)))] -= 1
    return y, p
    

"""
Tools to simulate embedding using J-UNIWARD steganographic algorithm
"""

SIGMA = 2 ** (-6)
NB_FILTERS = 3
WET_COST = 10 ** 13
TIME = 0


# Get 2D wavelet filters - Daubechies 8
# 1D high pass decomposition filter
HPDF = np.array([-0.0544158422, 0.3128715909, -0.6756307363, 0.5853546837,
                 0.0158291053, -0.2840155430, -0.0004724846, 0.1287474266,
                 0.0173693010, -0.0440882539, -0.0139810279, 0.0087460940,
                 0.0048703530, -0.0003917404, -0.0006754494, -0.0001174768]);
# 1D low pass decomposition filter
def compute_lpdf(hpdf):
    lpdf = np.zeros(hpdf.size)
    hpdf_flip = hpdf[::-1]
    for x in range(hpdf.size):
        lpdf[x] = ((-1) ** x) * hpdf_flip[x]
    return lpdf

LPDF = compute_lpdf(HPDF)

LPDF_m = np.matrix(LPDF)
HPDF_m = np.matrix(HPDF)

# Construction of 2D wavelet filters

F0= np.zeros((HPDF.size+1, HPDF.size+1))
F1= np.zeros((HPDF.size+1, HPDF.size+1))
F2= np.zeros((HPDF.size+1, HPDF.size+1))
F0[1:HPDF.size+1,1:HPDF.size+1] = np.dot(np.matrix.getT(LPDF_m), HPDF_m)
F1[1:HPDF.size+1,1:HPDF.size+1] = np.dot(np.matrix.getT(HPDF_m), LPDF_m)
F2[1:HPDF.size+1,1:HPDF.size+1] = np.dot(np.matrix.getT(HPDF_m), HPDF_m)


# Get embedding costs
class J_UNIWARD_process:
    """
    This class represents J_UNIWARD algorithm
    """

    def __init__(self, cover_path, payload):
        """
        Store the cover path and the payload
        """
        try:
            self.coverpath = cover_path
            self.payload = float(payload)
        except (ValueError):
            "Please enter a float for the payload"
            raise


    def dct2(self, x):
        return dct(dct(x, norm='ortho').T, norm='ortho').T

    def idct2(self, x):
        return idct(idct(x, norm='ortho').T, norm='ortho').T

    def pre_compute_impact_spatial_domain(self, c_quant):
        """
        Pre-compute impact in spatial domain when a jpeg coefficient is
        changed by 1
        """
        spatial_impact = [];
        for bcoord_i in range(8):
            for bcoord_j in range(8):
                test_coeffs = np.zeros((8, 8));
                test_coeffs[bcoord_i][bcoord_j] = 1;
                # dct (and inverse dct) is a linear transformation :
                # idct(coeff) * q = idct(coeff * q)
                # It is usefull to use q in order to reconstruct the dct
                # coefficient before quantization
                spatial_impact.append(self.idct2(test_coeffs)*c_quant[bcoord_i][bcoord_j]);
        return spatial_impact

    def pre_compute_impact_wavelet_coeff(self, spatial_impact, Fi):
        """
        Pre compute impact on wavelet coefficients when a jpeg coefficient is
        changed by 1
        """
        wavelet_impact = []
        for bcoord_i in range(8):
            for bcoord_j in range(8):
                # Maybe try with scipy.ndimage.correlate or scipy.misc.imfilter
                wavelet_impact.append(correlate(spatial_impact[8 * bcoord_i + bcoord_j], Fi[1:HPDF.size+1,1:HPDF.size+1], mode='full'));
        return wavelet_impact

    def pre_compute_impact_wavelet_coeff_suitability(self, spatial_impact):
        wavelet_impact0 =  self.pre_compute_impact_wavelet_coeff(spatial_impact, F0)
        wavelet_impact1 =  self.pre_compute_impact_wavelet_coeff(spatial_impact, F1)
        wavelet_impact2 =  self.pre_compute_impact_wavelet_coeff(spatial_impact, F2)
        return [wavelet_impact0, wavelet_impact1, wavelet_impact2]

    def compute_RC_suitability(self, c_spatial_padded):
        RC0 = correlate(c_spatial_padded, F0, mode = 'same')
        RC1 = correlate(c_spatial_padded, F1, mode = 'same')
        RC2 = correlate(c_spatial_padded, F2, mode = 'same')
        return [RC0, RC1, RC2]

    def compute_tempXi(self, rows_min, rows_max, cols_min, cols_max, index, RC, wavelet_impact):
        try:
            # Compute residual
            RC_sub = RC[rows_min:rows_max, cols_min:cols_max]
            # Get differences between cover and stego
            wav_cover_stego_diff = wavelet_impact[index]
            # Compute suitability
            tempXi = np.abs(wav_cover_stego_diff) / (np.abs(RC_sub)+ SIGMA)
            return tempXi
        except (IndexError, ValueError):
            print("Please verify filters used")
            raise

    def compute_cost(self, k, l, pad_size, RC, wi):
        """
        Computation of costs
        """
        rho = np.zeros((k, l));
        for row in range(k):
            for col in range(l):
                mod_row = row % 8
                mod_col = col % 8

                # for sub_rows_min and sub_cols_min, -1 is necessary to reach
                # the sub_rows_min-th and the sub_cols_min-th elements of RC[i]
                sub_rows_min = row-mod_row-6+pad_size - 1
                sub_rows_max = row-mod_row+16+pad_size
                sub_cols_min = col-mod_col-6+pad_size - 1
                sub_cols_max = col-mod_col+16+pad_size;

                index = 8 * mod_row + mod_col

                tempXi0 = self.compute_tempXi(sub_rows_min, sub_rows_max, sub_cols_min, sub_cols_max, index, RC[0], wi[0])
                tempXi1 = self.compute_tempXi(sub_rows_min, sub_rows_max, sub_cols_min, sub_cols_max, index, RC[1], wi[1])
                tempXi2 = self.compute_tempXi(sub_rows_min, sub_rows_max, sub_cols_min, sub_cols_max, index, RC[2], wi[2])

                rhoTemp = tempXi0 + tempXi1 + tempXi2
                rho[row, col] = np.sum(rhoTemp)

        return rho

    def JUNI(self, c_spatial, c_struct):
        """
        Compute costs according to cover in spatial and DCT domains
        """
        c_coeffs= c_struct.coef_arrays[0]
        c_quant = c_struct.quant_tables[0]
        spatial_impact = self.pre_compute_impact_spatial_domain(c_quant)
        [wavelet_impact0, wavelet_impact1, wavelet_impact2] = self.pre_compute_impact_wavelet_coeff_suitability(spatial_impact)

        # Add padding
        # By constructing, Fi are square-form
        # -1 is necessary because filters were widened
        pad_size = max(len(F0)-1, len(F1)-1, len(F2)-1)
        c_spatial_padded = np.pad(c_spatial, pad_size, 'symmetric')

        [RC0, RC1, RC2] = self.compute_RC_suitability(c_spatial_padded)

        [k,l]=c_coeffs.shape
        rho = self.compute_cost(k, l, pad_size, [RC0, RC1, RC2], [wavelet_impact0, wavelet_impact1, wavelet_impact2])

        # Adjust embedding costs
        rho[rho > WET_COST] = WET_COST # Threshold on the costs
        rho[np.isnan(rho)] = WET_COST # Check if all elements are numbers
        return rho

    def compute_rhos(self, c_spatial, c_struct):
        rho = self.JUNI(c_spatial, c_struct)

        rhoP1 = np.copy(rho)
        rhoM1 = np.copy(rho)

        c_coeffs= c_struct.coef_arrays[0]
        rhoP1[c_coeffs > 1023] = WET_COST # Do not embed +1 if the DCT coeff has max value
        rhoM1[c_coeffs < -1023] = WET_COST # Do not embed -1 if the DCT coeff has min value
        return rhoP1, rhoM1

# End of class

"""
Tools for ADV-EMB
"""

def ADV_EMB_procedure(params, iteration_step, neto2, grad2, input_layer, sess, image_path):
    
    '''
    INPUT 
    - params : all parameters
    - iteration_step : during attack of iteration_step = k, we fool classifier f^{k-1}
    - neto2 : the tensor which contains the probability output of the model
    - grad2 : the tensor which contains the gradient of the output probability wrt the input_layer
    - input_layer : the placeholder which is in input of the model
    - sess : the running session 
    - image_path : the name of the file like '1.jpg' or 'image1.npy'
    '''

    # Load the embedding costs for the DCT coeff
    image_path = image_path.split('.')[0]
    c_coeffs_cover = np.load(params.data_dir_cover + image_path + '.npy')
    nz_AC = np.sum(c_coeffs_cover!=0)-np.sum(c_coeffs_cover[::8,::8]!=0)
    c_coeffs_cover = c_coeffs_cover.flatten()
    rho = np.load(params.cost_dir + image_path + '.npy').reshape((1, params.image_size**2))
    rho = np.concatenate((np.copy(rho),np.copy(rho), np.zeros((1,params.image_size**2))),axis=0).reshape((3,params.image_size**2)) 
    perm = np.random.permutation(np.arange(params.image_size**2))
    directory_adv = params.data_dir_prot+'data_adv_'+str(iteration_step)+'/'

    beta=0
    
    while(beta<11):

        # EMBED COMMON GROUP
        if (beta==0):
            # Get the initial Stego image from Z^0
            c_coeffs_stego = np.load(params.data_dir_stego_0 + image_path + '.npy').flatten()

        elif(beta<10):

            c_coeffs_stego = np.copy(c_coeffs_cover)

            # 2) Two groups
            l1 = int(round(params.image_size**2*(1-float(beta)/10.)))
            l2 = params.image_size**2-l1
            com_gr, adj_gr = perm[:l1] , perm[l1:]

            # 3) Embedding simulation on common group
            c_coeffs_com_gr = c_coeffs_cover[com_gr]
            rho_com_gr = rho[:,com_gr]

            s_coeffs_com_gr, _= embedding_simulator(\
                c_coeffs_com_gr, rho_com_gr, round(params.emb_rate * nz_AC * (1-float(beta)/10)),True)
            c_coeffs_stego[com_gr] = s_coeffs_com_gr

        # COMPUTE GRADIENT
        lab = np.array([0 for beta in range(1)])

        perturb = sess.run(grad2, \
            {input_layer: c_coeffs_stego.reshape((1,params.image_size,params.image_size,1))})[0]
        perturb = perturb.reshape((params.image_size**2))

        # UPDATE THE COSTS
        if beta==0:
            adj_borne_sup = params.image_size**2

        l1 = int(round(params.image_size**2*(1-float(beta)/10.)))
        adj_gr = perm[l1:adj_borne_sup]

        booleen1 = np.zeros(params.image_size**2, dtype=bool)
        booleen1[adj_gr]=True # to select index in adj_gr

        booleen = np.asarray((perturb<0)*booleen1, dtype=bool)
        rho[0,booleen]/=params.alpha
        rho[1,booleen]*=params.alpha

        booleen = np.asarray((perturb>0)*booleen1, dtype=bool)
        rho[0,booleen]*=params.alpha
        rho[1,booleen]/=params.alpha

        adj_borne_sup=l1

        # 5) EMBED ADJUSTABLE GROUP
        if (beta>0):
            l1 = int(round(params.image_size**2*(1-float(beta)/10.)))
            l2 = params.image_size**2-l1
            com_gr, adj_gr = perm[:l1] , perm[l1:]

            c_coeffs_adj_gr = c_coeffs_cover[adj_gr]
            rho_adj_gr =rho[:, adj_gr]
        
            s_coeffs_adj_gr, _ = embedding_simulator(\
                c_coeffs_adj_gr, rho_adj_gr, round(params.emb_rate * nz_AC * float(beta)/10.),True)

            c_coeffs_stego[adj_gr] = s_coeffs_adj_gr

        # EVALUATE THE OUTPUT
        logits = sess.run(neto2, {input_layer: c_coeffs_stego.reshape((1,params.image_size,params.image_size,1))})
        y_pred = np.argmax(np.asarray(logits), 1)[0]


        if (y_pred==0 and beta==0): # z^0 already fools the classifier 
            beta_opt = 0
            np.save(directory_adv+'betas/'+image_path, beta_opt)
            return 

        elif (y_pred==0 and beta<11): # we succeeded in fooling the classifier with 0.<beta<=1.
            beta_opt = beta
            np.save(directory_adv+'adv_final/'+image_path, c_coeffs_stego.reshape((params.image_size,params.image_size)))
            np.save(directory_adv+'betas/'+image_path, beta_opt)
            return

        elif(beta==10 and y_pred==1): # even with beta=1, we didn't fooled the classifier
            # failure
            beta_opt = -1
            np.save(directory_adv+'betas/'+image_path,beta_opt)
            return
        
        else: # we didn't fool the classifier yet, let's increase beta from 0.1
            beta+=1

class procedure:

    def __init__(self, params, iteration_step, neto2, grad2, input_layer, sess):
        self.params = params
        self.iteration_step = iteration_step
        self.neto2 = neto2
        self.grad2 = grad2
        self.input_layer = input_layer
        self.sess = sess

    def apply(self, image_path):
        ADV_EMB_procedure(self.params, self.iteration_step, self.neto2, self.grad2, self.input_layer, self.sess, image_path)


def generate_adv_db(params, iteration_step, idx_start, idx_end):

    if params.model=='xunet':
        cnn_model_fn = cnn_model_fn_xu
    elif params.model=='srnet':
        cnn_model_fn = cnn_model_fn_sr

    # Restore cnn 
    input_layer = tf.placeholder(tf.float64, [1, params.image_size, params.image_size, 1], name='input_layer')

    files = np.load(params.permutation_files)
    files = files[idx_start:idx_end]

    neto1 = cnn_model_fn(input_layer, None, 'attack', params)
    neto2 = tf.nn.softmax(neto1)
    grad1 = tf.gradients(tf.reduce_sum(neto1[:,1]),input_layer)
    grad2 = tf.gradients(tf.reduce_sum(neto2[:,1]),input_layer)

    best_checkpoint = np.load(params.data_dir_prot+'train_f'+str(iteration_step-1)+'/best_checkpoint.npy')
    load_path = params.data_dir_prot+'train_f'+str(iteration_step-1)+'/'+str(best_checkpoint)  

    saver = tf.train.Saver()
    sess = tf.Session()
    saver.restore(sess, load_path) 

    prod = procedure(params, iteration_step, neto2, grad2, input_layer, sess) 

    print(prod, len(files))

    for x in files:
        print(x)
        prod.apply(x)

    sess.close()
    tf.reset_default_graph()
    
            