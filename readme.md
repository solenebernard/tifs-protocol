# You can find in this repository the code for running the protocol, and some trained classifiers during pre-trained protocol.

# To run the protocol 
## First step : Data preparation
- to save the DCT coefficients of your cover jpeg images in a "data_dir_cover" folder
 - to generate the symmetric costs map with J-UNIWARD and save it in a "data_dir_cost" folder
 - to generate the stegos given by the cost map of J-UNIWARD and with the emb_rate and save their DCT coefficients in a "data_dir_stego" folder

Launching the script --train_size : size of train. .py will compute those three numpy arrays for all jpeg images in data_dir.

### Example
python generate_data.py --data_dir=path_to_cover_JPEG_images
							--data_dir_cover=path_to_stego_images_jpeg
							--data_dir_stego_0=path_to_stego_images_jpeg
							--data_dir_cost=path_to_cost_maps
							--emb_rate=0.4

## Second step : The protocol

####  Python files 
- main.py : main script to start the protocol
- adv_emb_attack.py : needed functions to generate adversarial stego images
- evaluate_classif.py : functions to evaluate classifiers on data bases
- generate_train_db.py : functions to create to stego mix for training, depending on the strategy
- convert_tfrecords.py : functions to generate tfrecords files for training
- train.py :  to train the networks	

####  Numpy files 
- DCT_8.npy : kernel convolution layer used in the first layer of networks to decompress DCT coefficients of images  to compute spatial images
- DCT_4.npy : kernel of convolution layer used in XU-Net architecture
- c_quant_75.npy : quantification table for QF = 75
- c_quant_95.npy : quantification table for QF = 95
- c_quant_100.npy : quantification table for QF = 100
- permutation_files.npy : permutation of files which selects which images are in train, valid or test sets (take the first images to be in train, after in valid and last in test set)
- permutation_lignes.npy : permutation of some lines to make rotations and flips in jpeg domain (see data_augmentation in data_loader.py)

####  jpeg3_linux folder
jpeg library to load jpeg images, and after to save it in .tfrecords format (see convert_tfrecords.py)

Example of the command to run : 

## To execute once to save data in good format

Parameters of main.py : 
- --begin_step : Iteration for the start of the protocol. You should set it to 0 (so the first step)
- --number_steps : last iteration proceed during the protocol 
- --folder_model : path which should lead to the folder models, which contains needed constants. It contains also the "permutation_files.npy".
- --model : 'xunet' or 'srnet', which decides which classifier to use during protocol
- --data_dir_prot : main folder where to store all adversarial stegos and trained classifiers procuded during the protocol
- --data_dir_cover : DCT coefficients of cover images, produced during the first step
- --data_dir_stego_0 : DCT coefficients of initial stego images given by J-UNIWARD, produced during the first step
- --cost_dir : symmetrical cost maps given by J-UNIWARD, produced during the first step
- --image_size : size of squared images
- --QF : quantization factor, only 75 or 95. 
- --batch_size_classif : batch_size used during training. If it is set to 16, during training batches of size 16*2 will be feeded to classifiers, because it gives pairs of cover and stego.
- --batch_size_eval : batch_size used during evaluation 
- --emb_rate : the payload in bit per non zero AC coefficient, used to generate adversarial stego images
- --epoch_num : number of epochs to train the classifiers
- --strategy :  only 'minmax', 'last_it' or 'random' 
- --evaluate_every_epoch : when to save the classifiers
- --keep_prob : the probability of keeping variables for optional regularization with drop out. By default, there is no drop out so keep_prob is set to 1.
- --alpha : constant used by ADV-EMB for generating adversarial stegos. It is set to 2. like in the paper.
- --n_loops : number of loops only for the XU-Net architecture. In the paper of XU, the architecture is designed for 512 * 512 images, and the number of loops should be set to 5. But for 256*256 images, you should use 4 loops.
- --train_size : size of train set. 
- --valid_size : size of validation set. 
- --test_size : size of test set.


NB : This protocol was made for QF 75, 95 and 100. For more QF, you just have to save your quantization matrix in "models/" folder, like c_quant_75 (for QF 75), c_quant_95 (for QF 95) or c_quant_100 (for QF 100).

Careful : modify by hand the values in main.py of :
- params.boundaries : array of when to change the values of the learning rate (array of length n)
- params.values : array of values of learning rate between boundaries (array of length n+1)


# If you want to restore trained classifiers during an experiment

With the file restore.py you can load all trained models in GPU, and evaluate the output on the image you want (change the code for your custom image).

Some of parameters of restore.py are parameters used during training which should not be changed for evaluation. For each experiment, the parameters are shown below.

Trained models are named like 'model.ckpt_f*' : 'model.ckpt_f0' is the classifier trained at initialization of the protocol (between cover and J-UNIWARD) and model.ckpt_f1 at iteration 1, ...

The first parameter --iteration_step points out how many classifiers to load. If iteration_step=6, classifiers from f0 to f5 will be loaded.

####  Protocol at QF 100, image of size 512*512 for a payload of 0.4 bpnzAC and with XU-Net architecture.
Classifiers are stored in '/trained_xunet/experience_512_100_0_4/'. There are 6 classifiers in total : from iteration 0 to 5.
restore.py --iteration_step=6 --folder_model='./models/' --model='xunet' --load_dir='./trained_xunet/experience_512_100_0_4/' --image_size=512 --QF=100 --keep_prob=1. --n_loops=5 













