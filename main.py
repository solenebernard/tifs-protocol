#!/usr/bin/env python

import os
import time

import numpy as np
import argparse
import sys
from shutil import copy
import shutil
import os
import glob
from time import time, sleep

# Attack
from adv_emb_attack import *
# Convert
from convert_tf_records import *
# Training
from train import *
sys.path.append('./models/')
from SRNet import cnn_model_fn as cnn_model_fn_sr
from XUNet import cnn_model_fn as cnn_model_fn_xu

# Evaluation
from evaluate_classif import *
from generate_train_db import *


def concatenate_betas(params, iteration_step): 
    '''
    OUTPUT : None
    '''
    directory_adv = params.data_dir_prot+'data_adv_'+str(iteration_step)+'/'
    files = np.load(params.permutation_files)
    betas = np.zeros(len(files))
    for i,image_path in enumerate(files):
        try:
            betas[i] = np.load(directory_adv+'betas/'+image_path[:-4]+'.npy')
        except:
            betas[i] = -1
    np.save(directory_adv+'betas.npy',betas)
    # We than delete the folder beta
    shutil.rmtree(directory_adv+'betas/')

def create_folder(path, list_dir):
    '''
    INPUT : 
    - path : where to create a folder
    - list_dir : a list of folders' name to create
    OUTPUT : None
    '''
    for d in list_dir:
        if not os.path.exists(path+d):
            os.makedirs(path+d)

def run_iteration(params, iteration_step):

    if(iteration_step>0):

        ### GENERATE ADV DATA BASE OF THE LAST CLASSIFIER (ONE MODEL) ###
        directory_adv = params.data_dir_prot+'data_adv_'+str(iteration_step)+'/'
        create_folder(directory_adv, ['adv_final/', 'betas/'])
        generate_adv_db(params, iteration_step, 0, params.n_images-1)
        # After attacking, concatenate the betas used for the attack ADV-EMB for each image
        concatenate_betas(params, iteration_step)

        ### EVALUATION OF ALL THE CLASSIFIERS ON THE NEW ADV DATA BASE ###
        for i in range(iteration_step): 
            print('Evaluation of classifier f'+str(i)+' on data_adv_' + str(iteration_step))
            evaluate_step_i(params, iteration_f = i, iteration_adv = iteration_step)

        ### GENERATION OF THE TRAIN DATA BASE ###
        generate_train_db(params, iteration_step, params.strategy)
    
        ### CREATE TF RECORDS FILE FOR NEXT TRAINING ###
        print('Create the tfrecords files for the training of f '+str(iteration_step))
        convert_iteration(params, iteration_step)

    if(iteration_step==0):
        ### CREATE TF RECORDS FOR COVER AND INITIAL STEGOS ###
        print('Create the tfrecords files for the training of f '+str(iteration_step))
        convert_iteration(params, iteration_step)

    ### TRAINING A NEW CLASSIFIER ###
    print('Training ' + params.model + ' at iteration ' + str(iteration_step))
    
    # Create the folders
    dir_log = params.data_dir_prot + 'train_f'+str(iteration_step)+'/'
    paths = ['', 'pe_train/', 'pe_valid/' , 'pe_test/']
    create_folder(dir_log, paths)
    
    train_estimator(params, iteration_f = iteration_step) # train classifier
    choose_best_classifier(params, iteration_f = iteration_step) # Select best iteration

    ### EVALUATE NEW CLASSIFIER ON ALL STEGO DATA BASES AND ON COVER ###
    for i in range(-1, iteration_step+1): # -1 for cover
        print('Evaluation of classifier f_'+str(iteration_step)+' on data base ' + str(i))
        evaluate_step_i(params, iteration_f = iteration_step, iteration_adv = i)

    return(True)

    
if __name__ == '__main__':

    argparser = argparse.ArgumentParser(sys.argv[0])
    argparser.add_argument('--begin_step', type=int, default=0)
    argparser.add_argument('--number_steps', type=int, default=10)
    argparser.add_argument('--folder_model', type=str, help='The path to the folder where the architecture of models are saved')
    argparser.add_argument('--model', type=str, help='model in the protocol : xunet or srnet') # 'xunet' or 'srnet'

    argparser.add_argument('--data_dir_prot', type=str, 'Main folder where to save the images and classifiers computed during the protocol')
    argparser.add_argument('--data_dir_cover', type=str, help='Where are saved the DCT coefficients of cover')
    argparser.add_argument('--data_dir_stego_0', type=str, help='Where are saved the DCT coefficients of initial stegos')

    argparser.add_argument('--image_size', type=int)
    argparser.add_argument('--data_format', type=str, default='NHWC')
    argparser.add_argument('--QF', type=int)
    argparser.add_argument('--emb_rate', type=float)

    # FOR DATA TRAIN
    argparser.add_argument('--strategy', type=str, default='minmax') # minmax, random or lastit

    # FOR ADVERSARIAL EMBEDDING
    argparser.add_argument('--alpha', type=float, default=2.0)
    argparser.add_argument('--cost_dir',type=str)

    # FOR TRAINING
    argparser.add_argument('--batch_size_classif', type=int, default=16)
    argparser.add_argument('--batch_size_eval', type=int, default=16) 
    argparser.add_argument('--train_size', type=int, default=4000)
    argparser.add_argument('--valid_size', type=int, default=1000)
    argparser.add_argument('--test_size', type=int, default=5000)
    argparser.add_argument('--epoch_num', type=int, default=300)
    argparser.add_argument('--evaluate_every_epoch', type=int, default=10)
    argparser.add_argument('--keep_prob', type=float, default=1., help='Dropout parameter : what proportion of coefficient to keep')
    argparser.add_argument('--n_loops', type=int, default=5, help='Number of loops in the XU-Net architecture')
    argparser.add_argument('--num_of_threads', type=int, default=10, help='Number of threads for data loading')

    params = argparser.parse_args()
    params.n_images = params.train_size + params.valid_size + params.test_size
    params.permutation_files = params.folder_model + 'permutation_files.npy'
    
    n_iter_max = params.train_size*params.epoch_num//params.batch_size_classif
    f = int(512**2//(params.image_size**2))
    n = 16*5000*f//params.batch_size_classif

    if params.model=='xunet':
        params.cnn_model_fn = cnn_model_fn_xu
        params.boundaries = [n*i for i in range(1,(n_iter_max//n))] # boundaries : when to decrease the learning rate
        params.values = [0.9**i*0.001 for i in range(n_iter_max//n)] # how to decrease the learning rate
    elif params.model=='srnet':
        params.cnn_model_fn = cnn_model_fn_sr
        params.boundaries = [457000*f]
        params.values = [0.001,0.0001]

    iteration_step=params.begin_step    

    while iteration_step<params.number_steps+1:
        run_iteration(params, iteration_step)
        iteration_step+=1






        

