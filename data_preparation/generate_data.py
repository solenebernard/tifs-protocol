import sys, os
import numpy as np
import matplotlib.pyplot as plt
import copy
from matplotlib import rc
import argparse


rc('text', usetex=True)
rc('font',**{'family':'serif'})


from jpeg3_linux import jpeg


def ternary_entropy(p):
    #p0 = 1 - np.sum(p,axis=0)
    #p0[p0 < 0] = 1
    #P = np.vstack((p0,p))
    P = np.copy(p)
    P[P==0]=1
    H = -((P) * np.log2(P))
    Ht = np.sum(H)
    return Ht

def calc_lambda(rho, message_length, n):
    l3 = 1000 # just an initial value
    m3 = message_length + 1 # to enter at least one time in the loop, just an initial value
                            # m3 is the total entropy
    iterations = 0 # iterations counter
    while m3 > message_length:
        """
        This loop returns the biggest l3 such that total entropy (m3) <= message_length
        Stop when total entropy < message_length
        """
        l3 *= 2
        a = np.exp(-l3*(rho-np.min(rho,axis=0,keepdims=True)))
        p = a/(np.sum(a,axis=0))
        #pP1 = np.exp(-l3 * rhoP1) / (1 + np.exp(-l3 * rhoP1) + np.exp(-l3 * rhoM1))
        #pM1 = np.exp(-l3 * rhoM1) / (1 + np.exp(-l3 * rhoP1) + np.exp(-l3 * rhoM1))
    
        # Total entropy
        m3 = ternary_entropy(p)
        iterations += 1
        if iterations > 10:
            """
            Probably unbounded => it seems that we can't find beta such that
            message_length will be smaller than requested. Ternary search 
            doesn't work here
            """
            lbd = l3
            return lbd

    l1 = 0.0 # just an initial value
    m1 = n # just an initial value
    lbd = 0.0

    alpha = message_length / n # embedding rate
    # Limit search to 30 iterations
    # Require that relative payload embedded is roughly within
    # 1/1000 of the required relative payload
    while (m1 - m3) / n > alpha / 1000 and iterations < 30:
        lbd = l1 + (l3 - l1) / 2 # dichotomy
        #pP1 = np.exp(-lbd * rhoP1) / (1 + np.exp(-lbd * rhoP1) + np.exp(-lbd * rhoM1))
        #pM1 = np.exp(-lbd * rhoM1) / (1 + np.exp(-lbd * rhoP1) + np.exp(-lbd * rhoM1))
        a = np.exp(-lbd*(rho-np.min(rho,axis=0,keepdims=True)))
        p = a/(np.sum(a,axis=0))
        m2 = ternary_entropy(p) # total entropy new calculation

        if m2 < message_length: # classical ternary search
            l3 = lbd
            m3 = m2
        else:
            l1 = lbd
            m1 = m2
        iterations += 1 # for monitoring the number of iterations
    return lbd

def compute_proba(rho, message_length, n):
    """
    Embedding simulator simulates the embedding made by the best possible 
    ternary coding method (it embeds on the entropy bound). This can be 
    achieved in practice using Multi-layered syndrome-trellis codes (ML STC) 
    that are asymptotically approaching the bound
    """
    lbd = calc_lambda(rho, message_length, n)
    a = np.exp(-lbd*rho)
    p = a/(np.sum(a,axis=0))
    return(p)

def embedding_simulator(cover, rho, message_length):
    """
    Embedding simulator simulates the embedding made by the best possible 
    ternary coding method (it embeds on the entropy bound). This can be 
    achieved in practice using Multi-layered syndrome-trellis codes (ML STC) 
    that are asymptotically approaching the bound
    """

    n = cover.size
    p = compute_proba(rho, message_length, n)
    
    randChange = np.random.random_sample((cover.shape[0], cover.shape[1]))
    
    y = np.copy(cover)
    y[randChange<p[0]] += + 1
    y[np.logical_and(randChange >= p[0], randChange < (np.sum(p[:2],axis=0)))] -= 1
    
    return y, p

    
def create_hdf5(filename, buffer):
    p = h5py.File(filename,'w')
    p.create_dataset('dataset_1', data=buffer)
    p.close()

def dct2(x):
    return dct(dct(x, norm='ortho').T, norm='ortho').T

def idct2(x):
    return idct(idct(x, norm='ortho').T, norm='ortho').T

def compute_spatial_from_jpeg(jpeg_im,c_quant):
    """
    Compute the 8x8 DCT transform of the jpeg representation
    """
    w,h = jpeg_im.shape
    spatial_im = np.zeros((w,h))
    for bind_i in range(w//8):
        for bind_j in range(h//8):
            block = idct2(jpeg_im[bind_i*8:(bind_i+1)*8,bind_j*8:(bind_j+1)*8]*(c_quant))+128
            spatial_im[bind_i*8:(bind_i+1)*8,bind_j*8:(bind_j+1)*8] = block
    spatial_im = spatial_im.astype(np.float32)       
    return(spatial_im)

    
# -*- coding: utf-8 -*-

import os, sys
#from scipy.misc import imread
from scipy.signal import correlate
from scipy.fftpack import dct, idct
from time import time

"""
Tools to simulate embedding using J-UNIWARD steganographic algorithm
"""

SIGMA = 2 ** (-6)
NB_FILTERS = 3
WET_COST = 10 ** 13
TIME = 0


# Get 2D wavelet filters - Daubechies 8
# 1D high pass decomposition filter
HPDF = np.array([-0.0544158422, 0.3128715909, -0.6756307363, 0.5853546837,
                 0.0158291053, -0.2840155430, -0.0004724846, 0.1287474266,
                 0.0173693010, -0.0440882539, -0.0139810279, 0.0087460940,
                 0.0048703530, -0.0003917404, -0.0006754494, -0.0001174768]);
# 1D low pass decomposition filter
def compute_lpdf(hpdf):
    lpdf = np.zeros(hpdf.size)
    hpdf_flip = hpdf[::-1]
    for x in range(hpdf.size):
        lpdf[x] = ((-1) ** x) * hpdf_flip[x]
    return lpdf

LPDF = compute_lpdf(HPDF)

LPDF_m = np.matrix(LPDF)
HPDF_m = np.matrix(HPDF)

# Construction of 2D wavelet filters

F0= np.zeros((HPDF.size+1, HPDF.size+1))
F1= np.zeros((HPDF.size+1, HPDF.size+1))
F2= np.zeros((HPDF.size+1, HPDF.size+1))
F0[1:HPDF.size+1,1:HPDF.size+1] = np.dot(np.matrix.getT(LPDF_m), HPDF_m)
F1[1:HPDF.size+1,1:HPDF.size+1] = np.dot(np.matrix.getT(HPDF_m), LPDF_m)
F2[1:HPDF.size+1,1:HPDF.size+1] = np.dot(np.matrix.getT(HPDF_m), HPDF_m)


# Get embedding costs
class J_UNIWARD_process:
    """
    This class represents J_UNIWARD algorithm
    """

    def __init__(self, cover_path, payload):
        """
        Store the cover path and the payload
        """
        try:
            self.coverpath = cover_path
            self.payload = float(payload)
        except (ValueError):
            "Please enter a float for the payload"
            raise


    def dct2(self, x):
        return dct(dct(x, norm='ortho').T, norm='ortho').T

    def idct2(self, x):
        return idct(idct(x, norm='ortho').T, norm='ortho').T

    def pre_compute_impact_spatial_domain(self, c_quant):
        """
        Pre-compute impact in spatial domain when a jpeg coefficient is
        changed by 1
        """
        spatial_impact = [];
        for bcoord_i in range(8):
            for bcoord_j in range(8):
                test_coeffs = np.zeros((8, 8));
                test_coeffs[bcoord_i][bcoord_j] = 1;
                # dct (and inverse dct) is a linear transformation :
                # idct(coeff) * q = idct(coeff * q)
                # It is usefull to use q in order to reconstruct the dct
                # coefficient before quantization
                spatial_impact.append(self.idct2(test_coeffs)*c_quant[bcoord_i][bcoord_j]);
        return spatial_impact

    def pre_compute_impact_wavelet_coeff(self, spatial_impact, Fi):
        """
        Pre compute impact on wavelet coefficients when a jpeg coefficient is
        changed by 1
        """
        wavelet_impact = []
        for bcoord_i in range(8):
            for bcoord_j in range(8):
                # Maybe try with scipy.ndimage.correlate or scipy.misc.imfilter
                wavelet_impact.append(correlate(spatial_impact[8 * bcoord_i + bcoord_j], Fi[1:HPDF.size+1,1:HPDF.size+1], mode='full'));
        return wavelet_impact

    def pre_compute_impact_wavelet_coeff_suitability(self, spatial_impact):
        wavelet_impact0 =  self.pre_compute_impact_wavelet_coeff(spatial_impact, F0)
        wavelet_impact1 =  self.pre_compute_impact_wavelet_coeff(spatial_impact, F1)
        wavelet_impact2 =  self.pre_compute_impact_wavelet_coeff(spatial_impact, F2)
        return [wavelet_impact0, wavelet_impact1, wavelet_impact2]

    def compute_RC_suitability(self, c_spatial_padded):
        RC0 = correlate(c_spatial_padded, F0, mode = 'same')
        RC1 = correlate(c_spatial_padded, F1, mode = 'same')
        RC2 = correlate(c_spatial_padded, F2, mode = 'same')
        return [RC0, RC1, RC2]

    def compute_tempXi(self, rows_min, rows_max, cols_min, cols_max, index, RC, wavelet_impact):
        try:
            # Compute residual
            RC_sub = RC[rows_min:rows_max, cols_min:cols_max]
            # Get differences between cover and stego
            wav_cover_stego_diff = wavelet_impact[index]
            # Compute suitability
            tempXi = np.abs(wav_cover_stego_diff) / (np.abs(RC_sub)+ SIGMA)
            return tempXi
        except (IndexError, ValueError):
            print("Please verify filters used")
            raise

    def compute_cost(self, k, l, pad_size, RC, wi):
        """
        Computation of costs
        """
        rho = np.zeros((k, l));
        for row in range(k):
            for col in range(l):
                mod_row = row % 8
                mod_col = col % 8

                # for sub_rows_min and sub_cols_min, -1 is necessary to reach
                # the sub_rows_min-th and the sub_cols_min-th elements of RC[i]
                sub_rows_min = row-mod_row-6+pad_size - 1
                sub_rows_max = row-mod_row+16+pad_size
                sub_cols_min = col-mod_col-6+pad_size - 1
                sub_cols_max = col-mod_col+16+pad_size;

                index = 8 * mod_row + mod_col

                tempXi0 = self.compute_tempXi(sub_rows_min, sub_rows_max, sub_cols_min, sub_cols_max, index, RC[0], wi[0])
                tempXi1 = self.compute_tempXi(sub_rows_min, sub_rows_max, sub_cols_min, sub_cols_max, index, RC[1], wi[1])
                tempXi2 = self.compute_tempXi(sub_rows_min, sub_rows_max, sub_cols_min, sub_cols_max, index, RC[2], wi[2])

                rhoTemp = tempXi0 + tempXi1 + tempXi2
                rho[row, col] = np.sum(rhoTemp)

        return rho

    def JUNI(self, c_spatial, c_coeffs, c_quant):
        """
        Compute costs according to cover in spatial and DCT domains
        """
        spatial_impact = self.pre_compute_impact_spatial_domain(c_quant)
        [wavelet_impact0, wavelet_impact1, wavelet_impact2] = self.pre_compute_impact_wavelet_coeff_suitability(spatial_impact)

        # Add padding
        # By constructing, Fi are square-form
        # -1 is necessary because filters were widened
        pad_size = max(len(F0)-1, len(F1)-1, len(F2)-1)
        c_spatial_padded = np.pad(c_spatial, pad_size, 'symmetric')

        [RC0, RC1, RC2] = self.compute_RC_suitability(c_spatial_padded)

        [k,l]=c_coeffs.shape
        rho = self.compute_cost(k, l, pad_size, [RC0, RC1, RC2], [wavelet_impact0, wavelet_impact1, wavelet_impact2])

        # Adjust embedding costs
        rho[rho > WET_COST] = WET_COST # Threshold on the costs
        rho[np.isnan(rho)] = WET_COST # Check if all elements are numbers
        return rho

    def compute_rhos(self, c_spatial, c_coeffs, c_quant):
        rho = self.JUNI(c_spatial, c_coeffs, c_quant)

        rhoP1 = np.copy(rho)
        rhoM1 = np.copy(rho)

        #c_coeffs= c_struct.coef_arrays[0]
        rhoP1[c_coeffs > 1023] = WET_COST # Do not embed +1 if the DCT coeff has max value
        rhoM1[c_coeffs < -1023] = WET_COST # Do not embed -1 if the DCT coeff has min value
        return rhoP1, rhoM1


def compute_juni(params, image_path):

    # Open image 
    C = J_UNIWARD_process(params.data_dir + image_path, params.emb_rate)
    c_struct = jpeg(params.data_dir + image_path)
    cover = c_struct.coef_arrays[0]
    c_quant = c_struct.quant_tables[0]
    
    # Save cover
    np.save(params.data_dir_cover +image_path[:-4]+'.npy', np.asarray(cover,dtype=np.int16))

    # Compute cost map
    H,W = cover.shape
    c_spatial = compute_spatial_from_jpeg(cover, c_quant)
    rho, _ = C.compute_rhos(c_spatial, cover, c_quant)
    rho = rho.reshape((H,W))
    np.save(params.data_dir_cost+image_path[:-4]+'.npy', rho)

    # Generate stego
    nz_AC = np.sum(cover!=0)-np.sum(cover[::8,::8]!=0)
    message_length = nz_AC * params.emb_rate
    stego, _ = embedding_simulator(cover, rho, message_length)
    np.save(params.data_dir_stego_0+image_path[:-4]+'.npy', np.asarray(stego,dtype=np.int16))

    
if __name__ == '__main__':

    argparser = argparse.ArgumentParser(sys.argv[0])

    argparser.add_argument('--data_dir', type=str, help='Where are the cover jpeg images')

    argparser.add_argument('--data_dir_cover', type=str, help='Where to save the DCT coefficients of covers')
    argparser.add_argument('--data_dir_cost',type=str, help='Where to save the cost map')
    argparser.add_argument('--data_dir_stego_0', type=str, help='Where to save the DCT coefficients of stegos')

    argparser.add_argument('--emb_rate', type=float)

    params = argparser.parse_args()

    # Create the directories
    for path in [params.data_dir_cover, params.data_dir_cost, params.data_dir_stego_0]:
        if not os.path.exists(path):
            os.makedirs(path)
    
    l = os.listdir(params.data_dir)

    for image_path in l:
        print(image_path)
        compute_juni(params, image_path)









