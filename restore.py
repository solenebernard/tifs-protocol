import sys
import tensorflow as tf
import numpy as np
import argparse

sys.path.append('./models/')
from SRNet import cnn_model_fn as cnn_model_fn_sr
from XUNet import cnn_model_fn as cnn_model_fn_xu

def session_restore_cnn(params):
	input_layer = tf.placeholder(tf.float64, [None, params.image_size, params.image_size, 1], name='input_layer')
	labels = tf.placeholder(tf.int32, (None,))

	neto1, neto2, grad1, grad2, grad3 = [], [], [], [], []
	for i in range(params.iteration_step):
	    with tf.variable_scope('f'+str(i)):
	    	# Get the logit output
	        neto1.append(cnn_model_fn(input_layer, labels, 'attack', params))
	        neto2.append(tf.nn.softmax(neto1[-1]))
	        # Get the gradient output
	        grad1.append(tf.gradients(tf.reduce_sum(neto1[-1][:,1]),input_layer))
	        grad2.append(tf.gradients(tf.reduce_sum(neto2[-1][:,1]),input_layer))
	        grad3.append(tf.gradients(tf.gradients(grad2[-1][0],input_layer),input_layer))

	all_vars = tf.all_variables()
	model_vars = [[k for k in all_vars if k.name.startswith('f'+str(i)+'/')] for i in range(params.iteration_step)]

	saver = [tf.train.Saver(x) for x in model_vars]

	def restore_model(sess, iteration_f, saver):
	    load_path = params.load_dir+'model.ckpt_f'+str(iteration_f)
	    saver.restore(sess,load_path)

	sess = tf.Session(config=tf.ConfigProto(device_count = {'GPU':0}, log_device_placement=False))
	for i,x in enumerate(saver):
	    restore_model(sess, i, x)

	return(sess, input_layer, neto2)


if __name__ == '__main__':

    argparser = argparse.ArgumentParser(sys.argv[0])
    argparser.add_argument('--iteration_step', type=int, default=1)
    argparser.add_argument('--folder_model', type=str, help='The path to the folder where the architecture of models are saved')
    argparser.add_argument('--model', type=str, help='model in the protocol : xunet or srnet') # 'xunet' or 'srnet'
    argparser.add_argument('--load_dir', type=str, help='Main folder where to save the images and classifiers computed during the protocol')
    
    # Parameters of the experiment
    argparser.add_argument('--image_size', type=int)
    argparser.add_argument('--QF', type=int)

    # FOR TRAINING
    argparser.add_argument('--keep_prob', type=float, default=1., help='Dropout parameter : what proportion of coefficient to keep')
    argparser.add_argument('--data_format', type=str, default='NHWC')
    argparser.add_argument('--n_loops', type=int, default=5, help='Number of loops in the XU-Net architecture')
    
    params = argparser.parse_args()

    if params.model=='xunet':
        cnn_model_fn = cnn_model_fn_xu
    elif params.model=='srnet':
        cnn_model_fn = cnn_model_fn_sr

    sess, input_layer, probas = session_restore_cnn(params)

    # Evaluate the classifiers
    stego = np.zeros((1,params.image_size,params.image_size,1)) # To change
    p = np.asarray(sess.run(probas, {input_layer:stego}))
    print(p)


