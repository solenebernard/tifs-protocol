## CREATES 6 tfrecords files for train, valid and test, for cover and stego, from jpeg images

import numpy as np
import argparse
import os
import sys
import tensorflow as tf

def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def convert(params, iteration_step, mode, name):
	"""Converts a dataset of jpeg to tfrecords."""
	'''
	INPUT :
	- params : the parameters in input of the file 
	- mode (str) : 'train', 'valid' or 'test'
	- name  : 'cover' or 'stego'
	OUTPUT : 
	Save the file name + '_' + mode + '.tfrecords' in the folder created for the right iteration
	'''
	permutation_files = np.load(params.folder_model + 'permutation_files.npy') # saved permutation of files
	if(iteration_step>0):
		index = np.load(params.data_dir_prot + 'data_train_' + str(iteration_step) + '/index.npy')
	else:
		index = np.zeros(params.n_images, dtype=np.int16)

	if mode=='train': 
		images_path = permutation_files[:params.train_size]
		index = index[:params.train_size]

	elif mode=='valid':
		images_path = permutation_files[params.train_size:params.train_size+params.valid_size]
		index = index[params.train_size:params.train_size+params.valid_size]

	elif mode=='test':
		images_path = permutation_files[params.train_size+params.valid_size:params.train_size+params.valid_size+params.test_size]
		index = index[params.train_size+params.valid_size:params.train_size+params.valid_size+params.test_size]

	
	if (name=='stego'):
		save_path = params.data_dir_prot + 'data_train_' + str(iteration_step) + '/'

		if not os.path.exists(save_path):
			os.makedirs(save_path)

		filename = os.path.join(save_path, 'stego_' + mode + '.tfrecords')
		print('Writing', filename)
		with tf.python_io.TFRecordWriter(filename) as writer:
			for i,(file,ind) in enumerate(zip(images_path,index)):
				image_path = params.data_dir_prot + 'data_adv_' + str(ind) + '/adv_final/' + file[:-4] + '.npy'
				try:
					image = np.asarray(np.load(image_path),dtype=np.int16)
				except:
					image_path = params.data_dir_stego_0 + file[:-4] + '.npy'
					image = np.asarray(np.load(image_path),dtype=np.int16)
					
				image_raw = np.asarray(image, dtype=np.int16).tostring()
				example = tf.train.Example(
				features=tf.train.Features(
					feature={
						'name': _bytes_feature(bytes(file[:-4],'utf-8')),
						'label': _int64_feature(1),
						'image_raw': _bytes_feature(image_raw)
					}))
				writer.write(example.SerializeToString())

	elif (name=='cover'):
		save_path = params.data_dir_prot + 'data_train_cover/'

		if not os.path.exists(save_path):
			os.makedirs(save_path)

		filename = os.path.join(save_path, 'cover_' + mode + '.tfrecords')
		print('Writing', filename)
		with tf.python_io.TFRecordWriter(filename) as writer:
			for i,(file,ind) in enumerate(zip(images_path,index)):
				image_path = params.data_dir_cover + file[:-4] + '.npy'
				image = np.load(image_path)
				image_raw = np.asarray(image, dtype=np.int16).tostring()
				example = tf.train.Example(
				features=tf.train.Features(
					feature={
						'name': _bytes_feature(bytes(file[:-4],'utf-8')),
						'label': _int64_feature(0),
						'image_raw': _bytes_feature(image_raw)
					}))
				writer.write(example.SerializeToString())

def convert_data_set_stego(params, iteration_step):
	'''
	Build and save 3 .tfrecords files : stego for train, valid and test, from 
	the order of the array permutation_files.npy 
	'''
	for mode in ['train','valid','test']:
		convert(params, iteration_step, mode, 'stego')

def convert_data_set_cover(params):
	'''
	Build and save 3 .tfrecords files : cover for train, valid and test, from 
	the order of the array permutation_files.npy 
	'''
	for mode in ['train','valid','test']:
		convert(params, 0, mode, 'cover')

def convert_iteration(params, iteration_step):
	'''
	The aggregation of functions to convert to tf_records
	'''
	convert_data_set_stego(params, iteration_step)
	if(iteration_step==0): # Only convert cover for the initialization
		convert_data_set_cover(params)


	





	