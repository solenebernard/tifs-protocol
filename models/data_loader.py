
import os
import numpy as np
import sys
import tensorflow as tf
from functools import partial
import argparse
import time

def decode(serialized_example, params, mode='train'): # Read of .tfrecords file
    """Parses an image and label from the given `serialized_example`."""
    features = tf.io.parse_single_example(
        serialized_example,
        # Defaults are not specified since both keys are required.
        features={
            'name': tf.io.FixedLenFeature([], tf.string),
            'label': tf.io.FixedLenFeature([], tf.int64),
            'image_raw': tf.io.FixedLenFeature([], tf.string),
        })
    image = tf.io.decode_raw(features['image_raw'], tf.int16)
    image = tf.reshape(image, (params.image_size,params.image_size, 1))
    label = tf.cast(features['label'], tf.int32)
    return(image,label)

def input_fn(params, iteration_f, mode, name='cover'):

    data_dir_cover = params.data_dir_prot + 'data_train_cover/'
    data_dir_stego = params.data_dir_prot + 'data_train_' + str(iteration_f) + '/'

    with tf.name_scope('input'):

        if(mode == tf.estimator.ModeKeys.TRAIN or mode == tf.estimator.ModeKeys.EVAL):
            
            if(mode == tf.estimator.ModeKeys.TRAIN):
                m = 'train'
            
            elif(mode == tf.estimator.ModeKeys.EVAL):
                m = 'valid'
            
            filename_cover = data_dir_cover +'cover_'+m+'.tfrecords'
            filename_stego = data_dir_stego +'stego_'+m+'.tfrecords'
            dataset_cover = tf.data.TFRecordDataset(filename_cover)
            dataset_stego = tf.data.TFRecordDataset(filename_stego)
            dataset_cover = dataset_cover.map(lambda file:decode(file, params, mode))
            dataset_stego = dataset_stego.map(lambda file:decode(file, params, mode))
            dataset = tf.data.Dataset.zip((dataset_cover,dataset_stego))
            # Reorder
            dataset = dataset.map(lambda x,y : ((x[0],y[0]),(x[1],y[1])))
        
        elif (mode== tf.estimator.ModeKeys.PREDICT): # PREDICT mode = tf.estimator.ModeKeys.PREDICT
            # get the concatenation of train, valid and test for 'cover' or 'stego'
            if(name=='cover'):
                filename = [data_dir_cover + 'cover_'+m+'.tfrecords' for m in ['train','valid','test']]
            elif(name=='stego'):
                filename = [data_dir_stego + 'stego_'+m+'.tfrecords' for m in ['train','valid','test']]
            dataset = tf.data.TFRecordDataset(filename)
            dataset = dataset.map(lambda file:decode(file, params, mode))
        
        if(mode==tf.estimator.ModeKeys.TRAIN):
            dataset = dataset.repeat(params.epoch_num)
        
        dataset = dataset.batch(params.batch_size_classif)
        dataset = dataset.prefetch(buffer_size=params.num_of_threads)
        iterator = dataset.make_one_shot_iterator()
        images, labels = iterator.get_next()
        
        return images,labels






