import tensorflow as tf
import numpy as np
import random
import time
import os
from scipy import ndimage
#from PIL import Image
from tensorflow.python.framework import ops
from scipy.fftpack import dct, idct


def _conv2d(input_,shape_,strides_):
	return(tf.layers.conv2d(inputs=input_,\
        filters=shape_, \
        kernel_size=[3,3], \
        strides=strides_,\
        data_format='channels_last',\
        padding="SAME",\
        activation=None,\
        use_bias=False, \
        kernel_initializer=tf.random_normal_initializer(0, 0.01, dtype=tf.float64)))


def drop_prob(params, mode): # rate
	if mode == tf.estimator.ModeKeys.PREDICT or mode == tf.estimator.ModeKeys.EVAL or mode=='attack':
		return(0)
	elif mode == tf.estimator.ModeKeys.TRAIN:
		return(1-params.keep_prob)


def _conv_block_wsc(input_, shape_1, shape_2, mode, params):
	"""Convolutional layer with shortcut"""
	with tf.variable_scope('convm1'):
		conv1 = _conv2d(input_,shape_1,(1,1)) # convolution m.1
		conv1_bn = _batch_norm(conv1, mode) # batch norm m.1
		conv1_bn_relu = tf.nn.relu(conv1_bn) # ReLU m.1
		conv1_bn_relu = tf.nn.dropout(conv1_bn_relu, rate = drop_prob(params,mode))
	with tf.variable_scope('convm2'):
	    conv2 = _conv2d(conv1_bn_relu,shape_2,(2,2)) # convolution m.2
	    conv2_bn = _batch_norm(conv2,mode) # batch norm m.2
	with tf.variable_scope('conv_shortcut'):
	    conv3 = _conv2d(input_,shape_2,(2,2)) # convolution g
	    conv3_bn = _batch_norm(conv3,mode) # batch norm g
	with tf.name_scope('addition'):
		conv1_tot = conv2_bn+conv3_bn # Addition
	return(conv1_tot)


def _conv_block(input_, shape_, mode, params):
	"""Convolutional layer without shortcut"""
	with tf.variable_scope('input_relu'):
		relu = tf.nn.relu(input_) # ReLU m.1
	with tf.variable_scope('conv1'):
	    conv1 = _conv2d(relu,shape_,(1,1)) # convolution m.1
	    conv1_bn = _batch_norm(conv1,mode) # batch norm m.1
	    conv1_bn_relu = tf.nn.relu(conv1_bn) # ReLU m.1
	    conv1_bn_relu = tf.nn.dropout(conv1_bn_relu, rate = drop_prob(params,mode))
	with tf.variable_scope('conv2'):
	    conv2 = _conv2d(conv1_bn_relu,shape_,(1,1)) # convolution m.2
	    conv2_bn = _batch_norm(conv2,mode) # batch norm m.2
	with tf.name_scope('addition'):
		conv1_tot = conv2_bn+relu # Addition
	with tf.variable_scope('final_relu'):
		final_relu = tf.nn.relu(conv1_tot)
		final_relu = tf.nn.dropout(final_relu, rate = drop_prob(params,mode))
	return(final_relu)


def _batch_norm(input_, mode):
	if mode == tf.estimator.ModeKeys.PREDICT or mode == tf.estimator.ModeKeys.EVAL or mode=='attack':
		return(tf.layers.batch_normalization(input_, training=False))
	elif mode == tf.estimator.ModeKeys.TRAIN:
		return(tf.layers.batch_normalization(input_, training=True))

def cnn_model_fn(features, labels, mode, params):
	"""Model function for CNN."""
	
	if mode == tf.estimator.ModeKeys.TRAIN or mode == tf.estimator.ModeKeys.EVAL:
		
		input_layer_cover = features[0]
		input_layer_stego = features[1]
		input_layer = tf.concat([input_layer_cover,input_layer_stego],0)

		labels_cover = tf.zeros(params.batch_size_classif)
		labels_stego = tf.ones(params.batch_size_classif)
		labels_conc = tf.concat([labels_cover,labels_stego],0)
	
	else: 
		input_layer = features

	n = params.image_size//8
	input_layer = tf.cast(input_layer, tf.float64)
	
	# jpeg images to spatial images
	with tf.variable_scope('2D-IDCT'):

		# needed constants
		n = params.image_size//8
		c_quant = np.load(params.folder_model+'c_quant_'+str(params.QF)+'.npy') # 8*8 quantification matrix
		c_quant = np.tile(c_quant,(n,n)) # Tile it for the size of the image
		indices = np.hstack((np.array([i+n*k for k in range(8)]) for i in range(n))) # To reorder pixels in the output of convolution
		c_quant_t = tf.constant(c_quant,dtype=tf.float64,shape=[1,params.image_size,params.image_size,1])
		indices_t = tf.constant(indices,dtype=tf.int32,shape=[params.image_size])
		conv_DCT8 = tf.constant(np.load(params.folder_model+'DCT_8.npy'),shape=(8,8,1,64),dtype=tf.float64)
		
		# decompression via conv2d
		images = tf.multiply(input_layer, c_quant_t) # dequantization
		images = tf.nn.conv2d(input=images, \
						filter=conv_DCT8, \
						padding="VALID", \
						data_format='NHWC',\
						dilations=None, \
						name='pre_process', \
						strides=(1,8,8,1)) # 2D-IDCT
		
		# To reorder via tf.gather
		images=tf.transpose(images,(0,1,3,2), name='transposition')
		images=tf.reshape(images,(tf.shape(images)[0],params.image_size,params.image_size,1))
		spat_images = tf.gather(images,indices_t,axis=2, name='gather')
	
	# Data augmentation for training
	if(mode==tf.estimator.ModeKeys.TRAIN):
		
		with tf.variable_scope('data_augmentation'):
			
			def rotate(x, rot):
				# Rotate 0, 90, 180, 270 degrees
				return tf.image.rot90(x, rot)
			
			def flip(x,mir):
				f1 = lambda: tf.image.flip_left_right(x)
				f2 = lambda: x
				r = tf.case([(tf.greater(mir, tf.constant(0)), f1)], default=f2)
				return r
			
			batch_size = tf.shape(spat_images)[0]//2
			rot = tf.random.uniform([batch_size], minval=0, maxval=4, dtype=tf.int32)
			mir = tf.random.uniform([batch_size], minval=0, maxval=2, dtype=tf.int32)
			cover = tf.map_fn(fn=lambda t: rotate(t[0],t[1]), elems=(spat_images[:batch_size],rot), dtype='float64', name='rotate_cover')
			cover = tf.map_fn(fn=lambda t: flip(t[0],t[1]), elems=(cover,mir), dtype='float64', name='flip_cover')
			stego = tf.map_fn(fn=lambda t: rotate(t[0],t[1]), elems=(spat_images[batch_size:],rot), dtype='float64', name='rotate_stego')
			stego = tf.map_fn(fn=lambda t: flip(t[0],t[1]), elems=(stego,mir), dtype='float64', name='flip_stego')
			spat_images = tf.concat([cover,stego],0, name='concatenate_cover_stego')
			# Shuffle
			seed = np.random.randint(30000)
			spat_images = tf.random.shuffle(spat_images,seed=seed)
			labels_conc = tf.random.shuffle(labels_conc,seed=seed)
	
	# First layer : extract DCT4 features
	with tf.variable_scope('init'):

		DCT4 = np.load(params.folder_model + 'DCT_4.npy').astype(np.float64)
		conv_DCT4 = tf.constant(DCT4,shape=(4,4,1,16),dtype=tf.float64)
		paddings = tf.constant([[0, 0],[1, 1], [1, 1],[0, 0]])
		conv_DCT4 = tf.constant(DCT4,shape=(4,4,1,16),dtype=tf.float64)
		im_pad = tf.pad(spat_images, paddings, "CONSTANT")
		pre_process = tf.nn.convolution(\
		    input=im_pad, \
		    filter=conv_DCT4, \
		    padding="VALID", \
		    dilation_rate=None, \
		    name='pre_process')	
		output_abs=tf.abs(pre_process)
		x = tf.clip_by_value(\
		    output_abs, \
		    np.float64(0), #min_value
		    np.float64(8)) #max_value
	
	x = tf.cast(x,tf.float32)
	# n_loops*2 convolutions (n_loops with shortcuts, n_loops without)
	n_loops = params.n_loops
	for i in range(1,1+n_loops):
		with tf.variable_scope('conv_%d_block' % (i*2-1)):
			x = _conv_block_wsc(x, 12*(2**(i-1)),12*(2**i), mode, params)
		with tf.variable_scope('conv_%d_block' % (i*2)):
			x = _conv_block(x, 12*(2**i),mode, params)
	
	with tf.variable_scope('output'):
		w_s = int(params.image_size/2**n_loops) # size of the output depends on the number of loops
		x = tf.nn.pool(x,window_shape=[w_s,w_s],pooling_type='AVG',padding='VALID', name='pool')
		x = tf.contrib.layers.fully_connected(x,num_outputs=2,activation_fn=None, weights_regularizer = tf.keras.regularizers.l2(0.001), scope='fully_connected')

		logits = tf.reshape(x,[tf.shape(x)[0],2], name='reshape')
		y_pred = tf.argmax(input=logits, axis=1)
	
	predictions = {
			"logits": logits,
			"classes": y_pred,
			"probabilities": tf.nn.softmax(logits, name="softmax_tensor")
		}
	
	if mode=='attack':
		return(logits)
	
	elif mode == tf.estimator.ModeKeys.PREDICT:
		return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)
	
	labels_conc = tf.cast(labels_conc, tf.int32, name='cast')
	accuracy = tf.metrics.accuracy(labels=labels_conc, predictions=predictions['classes'],name ='accuracy')
	
	# Losses
	loss = tf.losses.sparse_softmax_cross_entropy(labels=labels_conc, logits=logits)
	reg_losses = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
	loss = tf.add_n([loss] + reg_losses)
	
	with tf.variable_scope('weights'):
		for var in tf.trainable_variables():
			tf.summary.histogram(var.name.replace(':','_'), var)
	
	# Configure the Training Op (for TRAIN mode)
	if mode == tf.estimator.ModeKeys.TRAIN:
		optimizer = tf.train.AdamOptimizer
		global_step = tf.train.get_or_create_global_step()
		learning_rate = tf.train.piecewise_constant(global_step, params.boundaries, params.values)
		optimizer = optimizer(learning_rate) 
		# Update batch norm
		update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
		
		with tf.variable_scope('metrics'):
			tf.summary.scalar('learning_rate', learning_rate)
			tf.summary.scalar('accuracy', accuracy[1]) # output to TensorBoard
			tf.summary.scalar('loss', loss)
			tf.summary.scalar('reg_losses', tf.add_n(reg_losses))
		
		with tf.control_dependencies(update_ops):
			train_op = optimizer.minimize(
				loss=loss,
				global_step=global_step)
		
		return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)
	
	elif mode == tf.estimator.ModeKeys.EVAL:
		# Define the metrics:
		eval_metric_ops = {"accuracy": accuracy}
		return tf.estimator.EstimatorSpec(
			mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)

