# Modules
import tensorflow as tf
import numpy as np
import random
import time
import os
from scipy import ndimage

from tensorflow.python.framework import ops
from tensorflow.contrib import layers
from tensorflow.contrib.framework import arg_scope
from functools import partial



def cnn_model_fn(features, labels, mode, params): # For feeding the network
    """Model function for CNN."""

    if mode == tf.estimator.ModeKeys.TRAIN or mode == tf.estimator.ModeKeys.EVAL:

        input_layer_cover = features[0]
        input_layer_stego = features[1]
        input_layer = tf.concat([input_layer_cover,input_layer_stego],0)

        labels_cover = tf.zeros(params.batch_size_classif)
        labels_stego = tf.ones(params.batch_size_classif)
        labels_conc = tf.concat([labels_cover,labels_stego],0)

    else: 
        input_layer = features
    
    # jpeg images to spatial images
    with tf.variable_scope('2D-IDCT'):

        # needed constants
        n = params.image_size//8
        c_quant = np.load(params.folder_model+'c_quant_'+str(params.QF)+'.npy') # 8*8 quantification matrix
        c_quant = np.tile(c_quant,(n,n)) # Tile it for the size of the image
        indices = np.hstack((np.array([i+n*k for k in range(8)]) for i in range(n))) # To reorder pixels in the output of convolution
        c_quant_t = tf.constant(c_quant,dtype=tf.float64,shape=[1,params.image_size,params.image_size,1])
        indices_t = tf.constant(indices,dtype=tf.int32,shape=[params.image_size])
        conv_DCT8 = tf.constant(np.load(params.folder_model+'DCT_8.npy'),shape=(8,8,1,64),dtype=tf.float64)
        
        # decompression via conv2d
        images = tf.multiply(input_layer, c_quant_t) # dequantization
        images = tf.nn.conv2d(input=images, \
                        filter=conv_DCT8, \
                        padding="VALID", \
                        data_format='NHWC',\
                        dilations=None, \
                        name='pre_process', \
                        strides=(1,8,8,1)) # 2D-IDCT
        
        # To reorder via tf.gather
        images=tf.transpose(images,(0,1,3,2), name='transposition')
        images=tf.reshape(images,(tf.shape(images)[0],params.image_size,params.image_size,1))
        spat_images = tf.gather(images,indices_t,axis=2, name='gather')


    if(mode==tf.estimator.ModeKeys.TRAIN):
        
        with tf.variable_scope('data_augmentation'):
            
            def rotate(x, rot):
                # Rotate 0, 90, 180, 270 degrees
                return tf.image.rot90(x, rot)
            
            def flip(x,mir):
                f1 = lambda: tf.image.flip_left_right(x)
                f2 = lambda: x
                r = tf.case([(tf.greater(mir, tf.constant(0)), f1)], default=f2)
                return r
            
            batch_size = tf.shape(spat_images)[0]//2
            rot = tf.random.uniform([batch_size], minval=0, maxval=4, dtype=tf.int32)
            mir = tf.random.uniform([batch_size], minval=0, maxval=2, dtype=tf.int32)
            cover = tf.map_fn(fn=lambda t: rotate(t[0],t[1]), elems=(spat_images[:batch_size],rot), dtype='float64', name='rotate_cover')
            cover = tf.map_fn(fn=lambda t: flip(t[0],t[1]), elems=(cover,mir), dtype='float64', name='flip_cover')
            stego = tf.map_fn(fn=lambda t: rotate(t[0],t[1]), elems=(spat_images[batch_size:],rot), dtype='float64', name='rotate_stego')
            stego = tf.map_fn(fn=lambda t: flip(t[0],t[1]), elems=(stego,mir), dtype='float64', name='flip_stego')
            spat_images = tf.concat([cover,stego],0, name='concatenate_cover_stego')
            # Shuffle
            seed = np.random.randint(30000)
            spat_images = tf.random.shuffle(spat_images,seed=seed)
            labels_conc = tf.random.shuffle(labels_conc,seed=seed)


    if params.data_format == 'NCHW':
        reduction_axis = [2,3]
        _inputs = tf.cast(tf.transpose(spat_images, [0, 3, 1, 2]), tf.float32)
    else:
        reduction_axis = [1,2]
        _inputs = tf.cast(spat_images, tf.float32)

    is_training = mode == tf.estimator.ModeKeys.TRAIN
                       
    with arg_scope([layers.conv2d], num_outputs=16,
                   kernel_size=3, stride=1, padding='SAME',
                   data_format=params.data_format,
                   activation_fn=None,
                   weights_initializer=layers.variance_scaling_initializer(),
                   weights_regularizer=layers.l2_regularizer(2e-4),
                   biases_initializer=tf.constant_initializer(0.2),
                   biases_regularizer=None),\
        arg_scope([layers.batch_norm],
                   decay=0.9, center=True, scale=True, 
                   updates_collections=None, is_training=is_training,
                   fused=True, data_format=params.data_format),\
        arg_scope([layers.avg_pool2d],
                   kernel_size=[3,3], stride=[2,2], padding='SAME',
                   data_format=params.data_format):
        with tf.variable_scope('Layer1'): 
            conv=layers.conv2d(_inputs, num_outputs=64, kernel_size=3)
            actv=tf.nn.relu(layers.batch_norm(conv))
        with tf.variable_scope('Layer2'): 
            conv=layers.conv2d(actv)
            actv=tf.nn.relu(layers.batch_norm(conv))
        with tf.variable_scope('Layer3'): 
            conv1=layers.conv2d(actv)
            actv1=tf.nn.relu(layers.batch_norm(conv1))
            conv2=layers.conv2d(actv1)
            bn2=layers.batch_norm(conv2)
            res= tf.add(actv, bn2)
        with tf.variable_scope('Layer4'): 
            conv1=layers.conv2d(res)
            actv1=tf.nn.relu(layers.batch_norm(conv1))
            conv2=layers.conv2d(actv1)
            bn2=layers.batch_norm(conv2)
            res= tf.add(res, bn2)
        with tf.variable_scope('Layer5'): 
            conv1=layers.conv2d(res)
            actv1=tf.nn.relu(layers.batch_norm(conv1))
            conv2=layers.conv2d(actv1)
            bn=layers.batch_norm(conv2)
            res= tf.add(res, bn)
        with tf.variable_scope('Layer6'): 
            conv1=layers.conv2d(res)
            actv1=tf.nn.relu(layers.batch_norm(conv1))
            conv2=layers.conv2d(actv1)
            bn=layers.batch_norm(conv2)
            res= tf.add(res, bn)
        with tf.variable_scope('Layer7'): 
            conv1=layers.conv2d(res)
            actv1=tf.nn.relu(layers.batch_norm(conv1))
            conv2=layers.conv2d(actv1)
            bn=layers.batch_norm(conv2)
            res= tf.add(res, bn)
        with tf.variable_scope('Layer8'): 
            convs = layers.conv2d(res, kernel_size=1, stride=2)
            convs = layers.batch_norm(convs)
            conv1=layers.conv2d(res)
            actv1=tf.nn.relu(layers.batch_norm(conv1))
            conv2=layers.conv2d(actv1)
            bn=layers.batch_norm(conv2)
            pool = layers.avg_pool2d(bn)
            res= tf.add(convs, pool)
        with tf.variable_scope('Layer9'):  
            convs = layers.conv2d(res, num_outputs=64, kernel_size=1, stride=2)
            convs = layers.batch_norm(convs)
            conv1=layers.conv2d(res, num_outputs=64)
            actv1=tf.nn.relu(layers.batch_norm(conv1))
            conv2=layers.conv2d(actv1, num_outputs=64)
            bn=layers.batch_norm(conv2)
            pool = layers.avg_pool2d(bn)
            res= tf.add(convs, pool)
        with tf.variable_scope('Layer10'): 
            convs = layers.conv2d(res, num_outputs=128, kernel_size=1, stride=2)
            convs = layers.batch_norm(convs)
            conv1=layers.conv2d(res, num_outputs=128)
            actv1=tf.nn.relu(layers.batch_norm(conv1))
            conv2=layers.conv2d(actv1, num_outputs=128)
            bn=layers.batch_norm(conv2)
            pool = layers.avg_pool2d(bn)
            res= tf.add(convs, pool)
        with tf.variable_scope('Layer11'): 
            convs = layers.conv2d(res, num_outputs=256, kernel_size=1, stride=2)
            convs = layers.batch_norm(convs)
            conv1=layers.conv2d(res, num_outputs=256)
            actv1=tf.nn.relu(layers.batch_norm(conv1))
            conv2=layers.conv2d(actv1, num_outputs=256)
            bn=layers.batch_norm(conv2)
            pool = layers.avg_pool2d(bn)
            res= tf.add(convs, pool)
        with tf.variable_scope('Layer12'): 
            conv1=layers.conv2d(res, num_outputs=512)
            actv1=tf.nn.relu(layers.batch_norm(conv1))
            conv2=layers.conv2d(actv1, num_outputs=512)
            bn=layers.batch_norm(conv2)
            avgp = tf.reduce_mean(bn, reduction_axis,  keep_dims=True )
    

    logits = layers.fully_connected(layers.flatten(avgp), num_outputs=2,
                        activation_fn=None, normalizer_fn=None,
                        weights_initializer=tf.random_normal_initializer(mean=0., stddev=0.01), 
                        biases_initializer=tf.constant_initializer(0.), scope='logits')

    predictions = {"classes": tf.argmax(input=logits, axis=1),
                    "probabilities": tf.nn.softmax(logits, name="softmax_tensor")}

    if mode=='attack':
        return(logits)

    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

    labels_conc = tf.cast(labels_conc, tf.int32, name='cast')
    accuracy = tf.metrics.accuracy(labels_conc, predictions['classes'], name ='accuracy')

    # Calculate Loss (for both TRAIN and EVAL modes)
    loss = tf.losses.sparse_softmax_cross_entropy(labels=labels_conc, logits=logits)    
    reg_losses = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
    loss = tf.add_n([loss] + reg_losses)


    # Configure the Training Op (for TRAIN mode)
    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = AdamaxOptimizer
        global_step = tf.train.get_or_create_global_step()
        learning_rate = tf.train.piecewise_constant(global_step, params.boundaries, params.values)
        tf.summary.scalar('learning_rate', learning_rate)
        optimizer = optimizer(learning_rate) 
        tf.summary.scalar('train_accuracy', accuracy[1]) # output to TensorBoard
        tf.summary.scalar('loss', loss) # output to TensorBoard
        tf.summary.scalar('reg_loss', reg_losses) # output to TensorBoard

        # Update batch norm
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            train_op = optimizer.minimize(
                loss=loss,
                global_step=global_step)
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

    # Add evaluation metrics (for EVAL mode)
    eval_metric_ops = {
      "accuracy": accuracy
    }
    return tf.estimator.EstimatorSpec(
        mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)



### Implementation of Adamax optimizer, taken from : https://github.com/openai/iaf/blob/master/tf_utils/adamax.py
from tensorflow.python.ops import control_flow_ops
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import state_ops
from tensorflow.python.framework import ops
from tensorflow.python.training import optimizer
import tensorflow as tf

class AdamaxOptimizer(optimizer.Optimizer):
    """
    Optimizer that implements the Adamax algorithm. 
    See [Kingma et. al., 2014](http://arxiv.org/abs/1412.6980)
    ([pdf](http://arxiv.org/pdf/1412.6980.pdf)).
    @@__init__
    """

    def __init__(self, learning_rate=0.001, beta1=0.9, beta2=0.999, use_locking=False, name="Adamax"):
        super(AdamaxOptimizer, self).__init__(use_locking, name)
        self._lr = learning_rate
        self._beta1 = beta1
        self._beta2 = beta2

        # Tensor versions of the constructor arguments, created in _prepare().
        self._lr_t = None
        self._beta1_t = None
        self._beta2_t = None

    def _prepare(self):
        self._lr_t = ops.convert_to_tensor(self._lr, name="learning_rate")
        self._beta1_t = ops.convert_to_tensor(self._beta1, name="beta1")
        self._beta2_t = ops.convert_to_tensor(self._beta2, name="beta2")

    def _create_slots(self, var_list):
        # Create slots for the first and second moments.
        for v in var_list:
            self._zeros_slot(v, "m", self._name)
            self._zeros_slot(v, "v", self._name)

    def _apply_dense(self, grad, var):
        lr_t = math_ops.cast(self._lr_t, var.dtype.base_dtype)
        beta1_t = math_ops.cast(self._beta1_t, var.dtype.base_dtype)
        beta2_t = math_ops.cast(self._beta2_t, var.dtype.base_dtype)
        if var.dtype.base_dtype == tf.float16:
            eps = 1e-7  # Can't use 1e-8 due to underflow -- not sure if it makes a big difference.
        else:
            eps = 1e-8

        v = self.get_slot(var, "v")
        v_t = v.assign(beta1_t * v + (1. - beta1_t) * grad)
        m = self.get_slot(var, "m")
        m_t = m.assign(tf.maximum(beta2_t * m + eps, tf.abs(grad)))
        g_t = v_t / m_t

        var_update = state_ops.assign_sub(var, lr_t * g_t)
        return control_flow_ops.group(*[var_update, m_t, v_t])

    def _apply_sparse(self, grad, var):
        raise NotImplementedError("Sparse gradient updates are not supported.")


    



