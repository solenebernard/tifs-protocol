import numpy as np
import argparse
import sys
from shutil import copy
import os
import glob
from time import time
from statsmodels.distributions.empirical_distribution import ECDF

def generate_train_db(params, iteration_step, strategy):

    files = np.load(params.folder_model + 'permutation_files.npy')

    if strategy=='minmax':
        # Probas = Probas(image==stego==1)
        probas = np.zeros((iteration_step+1,iteration_step,len(files))) # nb_data_bases * nb_classifs * n_images
        # lignes * colonnes * profondeur

        #CALIBRATION
        ecdf_list=[]
        for i in range(iteration_step):
            ecdf = ECDF(np.load(params.data_dir_prot+'cover/eval_f'+str(i)+'/probas.npy'))
            probas[0,i]=ecdf(np.load(params.data_dir_prot+'data_adv_0/eval_f'+str(i)+'/probas.npy'))
            ecdf_list.append(ecdf)

        for j in range(1,iteration_step+1): 
            for i in range(iteration_step):
                ecdf = ecdf_list[i]
                probas[j,i]=ecdf(np.load(params.data_dir_prot+'data_adv_'+str(j)+'/eval_f'+str(i)+'/probas.npy'))

        index = np.argmin(np.max(probas,axis=1),axis=0)

    elif strategy=='random':
        index = np.random.randint(iteration_step+1, size=len(files))

    elif strategy=='lastit':
        index = np.zeros(len(files),dtype=int)+iteration_step

    if not os.path.exists(params.data_dir_prot+'data_train_'+str(iteration_step)+'/'):
        os.makedirs(params.data_dir_prot+'data_train_'+str(iteration_step)+'/')

    np.save(params.data_dir_prot+'data_train_'+str(iteration_step)+'/index',index)
    return(index)
