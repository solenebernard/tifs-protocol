import tensorflow as tf
import os
import argparse
import sys
from functools import partial
import time
import glob
from sklearn.metrics import accuracy_score

import numpy as np
sys.path.append('models/')
from data_loader import input_fn


def getLatestGlobalStep(LOG_DIR):
    # no directory
    if not os.path.exists(LOG_DIR):
        return 0
    checkpoints = [int(f.split('-')[-1].split('.')[0]) \
                   for f in os.listdir(LOG_DIR) if (f.startswith('model.ckpt') and 'temp' not in f)]
    # no checkpoint files
    if not checkpoints:
        return 0
    global_step = max(checkpoints)
    to_file = open(LOG_DIR+'checkpoint', 'w')
    line = 'model_checkpoint_path: "model.ckpt-{}"'.format(global_step)
    to_file.write(line)
    to_file.close()
    return global_step

def updateCheckpointFile(LOG_DIR, checkpoint_name):
    if not os.path.isfile(LOG_DIR + 'checkpoint'):
        return 0
    from_file = open(LOG_DIR+'checkpoint')
    line = from_file.readline()
    from_file.close()
    splits = line.split('"')
    new_line = splits[0] + '"' + checkpoint_name + '"' + splits[-1]
    to_file = open(LOG_DIR + 'checkpoint', mode="w")
    to_file.write(new_line)
    to_file.close()
    
def deleteCheckpointFile(LOG_DIR):
    if os.path.isfile(LOG_DIR + 'checkpoint'):
        os.remove(LOG_DIR + 'checkpoint')

def choose_best_classifier(params, iteration_f):
    dir_log = params.data_dir_prot + 'train_f'+str(iteration_f)+'/'

    liste = glob.glob(dir_log + 'model.ckpt-*.index')
    liste = [int(x.split('/')[-1].split('-')[1].split('.')[0]) for x in liste] 
    liste.sort()

    pe_train_liste = np.zeros((len(liste),1))
    pe_valid_liste = np.zeros((len(liste),1))
    pe_test_liste = np.zeros((len(liste),1))
    
    index_valid_best = 0
    pe_train_best = 1.0
    pe_valid_best = 1.0
    pe_test_best = 1.0
    for i,x in enumerate(liste):
        pe_train, pe_valid, pe_test = p_error(params, iteration_f, x)
        pe_train_liste[i,0] = pe_train
        pe_valid_liste[i,0] = pe_valid
        pe_test_liste[i,0] = pe_test
        if pe_valid<pe_valid_best:
            index_valid_best = x
            pe_train_best = pe_train
            pe_valid_best = pe_valid
            pe_test_best = pe_test

    np.save(dir_log+'best_checkpoint.npy','model.ckpt-'+str(index_valid_best))
    np.save(dir_log+'pe_valid/'+str(index_valid_best)+'.npy',pe_valid_best)
    np.save(dir_log+'pe_test/'+str(index_valid_best)+'.npy',pe_test_best)
    liste = np.asarray(liste)
    np.save(dir_log+'pe_train/tableau.npy',np.concatenate((liste.reshape((len(liste),1)),pe_train_liste),axis=1))
    np.save(dir_log+'pe_valid/tableau.npy',np.concatenate((liste.reshape((len(liste),1)),pe_valid_liste),axis=1))
    np.save(dir_log+'pe_test/tableau.npy',np.concatenate((liste.reshape((len(liste),1)),pe_test_liste),axis=1))


def p_error(params, iteration_f, index_model):
    dir_log = params.data_dir_prot + 'train_f'+str(iteration_f)+'/'

    load_checkpoint = dir_log + 'model.ckpt-'+str(index_model)
    predictions_cover, predictions_stego = predict_estimator(params, iteration_f, load_checkpoint, mode=tf.estimator.ModeKeys.PREDICT)
    predictions_cover, predictions_stego = predictions_cover[:,1], predictions_stego[:,1]
    train_size, valid_size, test_size = params.train_size, params.valid_size, params.test_size
    
    thresholds = np.linspace(0,1,51)[1:-1]
    PE_train, PE_valid, PE_test = [], [], []
    
     # Choose best threshold
    for t in thresholds:
        pred_cover, pred_stego = predictions_cover[:4000], predictions_stego[:4000]
        fa = len(pred_cover[pred_cover>t])*1.0/len(pred_cover)
        md = len(pred_stego[pred_stego<=t])*1.0/len(pred_stego)
        pe_train = (fa+md)/2

        pred_cover, pred_stego = predictions_cover[4000:5000], predictions_stego[4000:5000]
        fa = len(pred_cover[pred_cover>t])*1.0/len(pred_cover)
        md = len(pred_stego[pred_stego<=t])*1.0/len(pred_stego)
        pe_valid = (fa+md)/2
        #np.save(params.dir_log+'pe_valid/'+str(index_model)+'.npy',pe_valid)

        pred_cover, pred_stego = predictions_cover[5000:], predictions_stego[5000:]
        fa = len(pred_cover[pred_cover>t])*1.0/len(pred_cover)
        md = len(pred_stego[pred_stego<=t])*1.0/len(pred_stego)
        pe_test = (fa+md)/2

        PE_train.append(pe_train)
        PE_valid.append(pe_valid)
        PE_test.append(pe_test)

    PE_valid = np.asarray(PE_valid)
    mini = np.argmin(PE_valid)
    return(PE_train[mini], PE_valid[mini], PE_test[mini])
    

def predict_estimator(params, iteration_f, load_checkpoint, mode=tf.estimator.ModeKeys.PREDICT):
    
    dir_log = params.data_dir_prot + 'train_f'+str(iteration_f)+'/'

    xu_classifier = tf.estimator.Estimator(
        model_fn=params.cnn_model_fn, model_dir=dir_log,
        params=params)
    input_fn_cover = partial(input_fn, params, iteration_f, mode, 'cover')
    input_fn_stego = partial(input_fn, params, iteration_f, mode, 'stego')
    predictions_cover = list(xu_classifier.predict(input_fn_cover, predict_keys='probabilities', checkpoint_path=load_checkpoint))
    predictions_stego = list(xu_classifier.predict(input_fn_stego, predict_keys='probabilities', checkpoint_path=load_checkpoint))
    predictions_cover = np.array([p['probabilities'] for p in predictions_cover])
    predictions_stego = np.array([p['probabilities'] for p in predictions_stego])
    return (predictions_cover, predictions_stego)
    
def train_estimator(params, iteration_f, load_checkpoint=None):

    dir_log = params.data_dir_prot + 'train_f'+str(iteration_f)+'/'

    # Create the Estimator
    xu_classifier = tf.estimator.Estimator(
        model_fn=params.cnn_model_fn, model_dir=dir_log,
        params=params,
        config=tf.estimator.RunConfig(save_summary_steps=params.evaluate_every_epoch*params.train_size/params.batch_size_classif,
                                      save_checkpoints_steps=params.evaluate_every_epoch*params.train_size/params.batch_size_classif,
                                      session_config=tf.ConfigProto(log_device_placement=False),
                                      keep_checkpoint_max=10000))
    if load_checkpoint == 'last':
        start = getLatestGlobalStep(dir_log)
    elif load_checkpoint is not None:
        start = int(load_checkpoint.split('-')[-1])
        updateCheckpointFile(dir_log, load_checkpoint)
    else:
        start = 0 
        deleteCheckpointFile(dir_log)
    print('global step: ', start)

    input_fn_train = partial(input_fn, iteration_f = iteration_f, mode=tf.estimator.ModeKeys.TRAIN, params=params)
    input_fn_evaluate = partial(input_fn, iteration_f = iteration_f, mode=tf.estimator.ModeKeys.EVAL, params=params)

    train_spec = tf.estimator.TrainSpec(input_fn = input_fn_train)
    eval_spec = tf.estimator.EvalSpec(input_fn = input_fn_evaluate)
    tf.estimator.train_and_evaluate(xu_classifier, train_spec, eval_spec)

    



    
